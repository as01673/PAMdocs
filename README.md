Welcome to the documentation to PAM - PIE Analysis with MATLAB
=================================================================

This repository hosts the uncompiled documentation.

If you were looking for the manual to PAM, you will find it at http://pam.readthedocs.io!