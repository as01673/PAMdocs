Glossary
#########

.. glossary::
    :sorted:

    APBS
        All photon burst search

    DCBS
        Dual channel burst search

    PIE
        Pulsed interleaved excitation

    MFD
        Multiparameter fluorescence detection, i.e. the simultaneous detection of color, fluorescence lifetime and anisotropy information

    TAC
        Time-to-amplitude converter as used in TCSPC electronics.

    TAC range
        The length of the available microtime range, usually given by the inverse of the laser repetition or synchronization rate.

    TAC channel
        Unit of the microtime. Related to microtime in nanoseconds by the resolution of the TCSPC electronics.

    TCSPC
        Time-correlated single photon counting

    TTTR
        Time-tagged time-resolved data. PicoQuant's name for TCSPC data.
