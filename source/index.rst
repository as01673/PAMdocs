.. PAMdocs documentation master file, created by Anders Barth

Welcome to PAM's documentation!
===================================

**PAM** is short for *PIE Analysis with MATLAB* - it is a GUI-based software package
for the analysis of fluorescence experiments and supports a large number of analysis methods.
While originally designed for *pulsed-interleaved excitation* (PIE, [Müller2005]_) experiments, it now has
a wide support for the most commonly used fluorescence experiments.

Currently implemented:

* Fluorescence correlation spectroscopy and its derivatives

  * FCS/FCCS/FLCS/fFCS

* Single-molecule FRET spectroscopy (burst analysis), including:

  * Photon distribution analysis for two- and three-color FRET experiments
  * species-selective filtered fluorescence correlation spectroscopy 

* Image correlation spectroscopy, including:

  * ICS/RICS/TICS/STICS
  * ratser lifetime image correlation spectroscopy (RLICS)
  * support for arbitrary region image correlation spectroscopy (ARICS)

* Fluorescence lifetime imaging (PhasorFLIM)
* TCSPC and time-resolved anisotropy analysis

.. toctree::
   :maxdepth: 1

   GettingStarted.rst
   pam.rst
   fcsfit.rst
   taufit.rst
   mia.rst
   miafit.rst
   phasor.rst
   burstbrowser.rst
   pda.rst
   tcPDA.rst
   particledetection.rst
   paircorrelation.rst


.. [Lamb2000] Lamb, D. C., Schenk, A., Röcker, C., Scalfi-Happ, C. & Nienhaus, G. U. Sensitivity Enhancement in Fluorescence Correlation Spectroscopy of Multiple Species Using Time-Gated Detection. Biophys J 79, 1129–1138 (2000)
.. [Müller2005] Müller, B. K., Zaychikov, E., Bräuchle, C. & Lamb, D. C. Pulsed interleaved excitation. Biophys J 89, 3508–3522 (2005)
.. [Kudryavtsev2012] Kudryavtsev, V. et al. Combining MFD and PIE for Accurate Single-Pair Förster Resonance Energy Transfer Measurements. ChemPhysChem 13, 1060–1078 (2012)
.. [Hendrix2013] Hendrix, J., Schrimpf, W., Höller, M. & Lamb, D. C. Pulsed Interleaved Excitation Fluctuation Imaging. Biophys J 105, 848–861 (2013)
.. [Hendrix2016] Hendrix, J., Dekens, T., Schrimpf, W. & Lamb, D. C. Arbitrary-Region Raster Image Correlation Spectroscopy. Biophys J 111, 1785–1796 (2016)
